using FluentMigrator.Runner;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PruebaMultiTenant.Api.Middlewares;
using PruebaMultiTenant.Infrastucture.Contexts;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<OrganizationsUsersContext>(options =>
{
    options.UseSqlite(builder.Configuration.GetConnectionString("OrganizationsUsersConnection"));
});

builder.Services.AddDbContext<ProductsContext>(options =>
{
    options.UseSqlite(builder.Configuration.GetConnectionString("ProductsConnection"));
});


builder.Services.AddAuthentication("authScheme")
    .AddJwtBearer("authScheme", options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = false,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["AppSettings:Token"])),
        };
    });


var app = builder.Build();




// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseMiddleware<MultitenantMiddleware>();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
