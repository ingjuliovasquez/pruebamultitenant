﻿using Microsoft.EntityFrameworkCore;
using PruebaMultiTenant.Domain.Models;

namespace PruebaMultiTenant.Infrastucture.Contexts
{
    public class OrganizationsUsersContext : DbContext
    {
        public OrganizationsUsersContext(DbContextOptions<OrganizationsUsersContext> options) : base(options)
        {
        }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
