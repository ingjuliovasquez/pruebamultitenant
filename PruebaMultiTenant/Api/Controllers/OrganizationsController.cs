﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Common.Migrations;
using PruebaMultiTenant.Domain.Models;
using PruebaMultiTenant.Infrastucture.Contexts;

namespace PruebaMultiTenant.Api.Controllers
{
    [Authorize(AuthenticationSchemes = "authScheme")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsController : ControllerBase
    {
        private readonly OrganizationsUsersContext _organizationsUsersContext;
        private readonly IMigrationRunner _migrationRunner;

        public OrganizationsController(OrganizationsUsersContext context)
        {
            _organizationsUsersContext = context;
        }


        [HttpPost]
        public async Task<ActionResult<Organization>> PostOrganization(OrganizationDto organization)
        {
            if (_organizationsUsersContext.Organizations == null)
            {
                return Problem("Entity set 'OrganizationsUsersContext.Organizations'  is null.");
            }
            Organization _org = new Organization()
            {
                Id = 0,
                Nombre = organization.Nombre,
            };
            _organizationsUsersContext.Organizations.Add(_org);
            await _organizationsUsersContext.SaveChangesAsync();

            return CreatedAtAction("GetOrganization", new { id = _org.Id }, _org);
        }



        // GET: api/Organizations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Organization>>> GetOrganizations()
        {
          if (_organizationsUsersContext.Organizations == null)
          {
              return NotFound();
          }
            return await _organizationsUsersContext.Organizations.ToListAsync();
        }

        // GET: api/Organizations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Organization>> GetOrganization(int id)
        {
          if (_organizationsUsersContext.Organizations == null)
          {
              return NotFound();
          }
            var organization = await _organizationsUsersContext.Organizations.FindAsync(id);

            if (organization == null)
            {
                return NotFound();
            }

            return organization;
        }

        // PUT: api/Organizations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrganization(int id, Organization organization)
        {
            if (id != organization.Id)
            {
                return BadRequest();
            }

            _organizationsUsersContext.Entry(organization).State = EntityState.Modified;

            try
            {
                await _organizationsUsersContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrganizationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        

        // DELETE: api/Organizations/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrganization(int id)
        {
            if (_organizationsUsersContext.Organizations == null)
            {
                return NotFound();
            }
            var organization = await _organizationsUsersContext.Organizations.FindAsync(id);
            if (organization == null)
            {
                return NotFound();
            }

            _organizationsUsersContext.Organizations.Remove(organization);
            await _organizationsUsersContext.SaveChangesAsync();

            return NoContent();
        }

        private bool OrganizationExists(int id)
        {
            return (_organizationsUsersContext.Organizations?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
