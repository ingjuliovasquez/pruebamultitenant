﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
namespace PruebaMultiTenant.Api.Middlewares
{
    public class MultitenantMiddleware
    {
        private readonly RequestDelegate _next;

        public MultitenantMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            // Segmento del path que contiene el tenant (organización)
            string path = context.Request.Path.Value;
            string[] segments = path.Split('/');

            if (segments.Length > 1)
            {
                // El primer segmento del path es el tenant
                string tenant = segments[1];

                // Aquí puedes realizar la lógica para determinar la organización actual
                // y configurar la cadena de conexión correspondiente

                // Por ejemplo, puedes buscar la organización en la base de datos y obtener la cadena de conexión
                // Configura la cadena de conexión en el contexto del Entity Framework

                // context.GetDbContext<OrganizationsUsersContext>().ChangeDatabase(tenant);

                // Se elimina el segmento del path para que no afecte las rutas posteriores
                context.Request.Path = context.Request.Path.Value.Substring(segments[1].Length + 1);
            }

            await _next(context);
        }
    }
}
