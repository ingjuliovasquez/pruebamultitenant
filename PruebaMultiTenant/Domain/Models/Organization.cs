﻿namespace PruebaMultiTenant.Domain.Models
{
    public class Organization
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
